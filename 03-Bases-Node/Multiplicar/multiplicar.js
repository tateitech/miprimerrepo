const fs = require('fs');
var colors = require('colors');

let listaTabla = (base, limite) => {

    console.log('======================='.green)
    console.log(`===tabla de ${base}`.green)
    console.log('================='.green)
    console.log('======================='.green)

    for (let i = 0; i <= limite; i++) {
        console.log(`${base} * ${i} = ${base * i}`)

    }

}

let crearArchivo = (base, limite) => {
        return new Promise((resolve, reject) => {
                    let data = '';

                    for (let i = 0; i < limite; i++) {
                        data += `${ base } + ${ i } = ${ base * i} \n`
                    }

                    fs.writeFile(`Tablas/tabla-${ base }-al-${limite}.txt`, data, (err) => {
                                if (err) {
                                    reject(err)
                                } else {
                                    resolve(`el archivo ${colors.magenta( `tabla-${ base }.txt `)} fue sido creado!`);
            }
        });
    })
}

module.exports = {
    crearArchivo,
    listaTabla
}