//const argv = require('yargs').argv hace referencia  aun paquete por eso no se le indica la ruta

const argv = require('./config/yargs').argv;
const { crearArchivo, listaTabla } = require('./Multiplicar/multiplicar');

let comando = argv._[0];

switch (comando) {
    case 'listar':
        listaTabla(argv.base, argv.limite);
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then((resp) => { console.log(resp) })
            .catch(e => console.log(e))
}

/*crearArchivo(base)
    .then((resp) => { console.log(resp) })*/